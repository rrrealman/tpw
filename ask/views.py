from ask.models import Question, Profile, User
from pprint import pformat

# Create your views here.

def get_qs():
	# if qs_exist_in_cashe:
	# 	qs = qs_from_cashe
	# else:
	# 	qs = Question.objects.all()
	# 	save_to_cashe(qs)
	return Question.objects.all()

def get_ms():
	return Profile.objects.all()

def by_dcrs_ms():
	return get_ms().order_by('-rating')