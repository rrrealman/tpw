from __future__ import absolute_import

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from django.db.models import Min, Max

from ask.models import *

from faker.frandom import random
from faker.lorem import sentence, sentences
from mixer.fakers import get_username, get_email
from pprint import pformat
from optparse import make_option

class Command(BaseCommand):

	help = 'Generate and load fake data to database test'

	option_list = BaseCommand.option_list + (
        make_option('--users',
            action='store',
            dest='users',
            default=0,
            help='Number of users to generate'),

        make_option('--questions',
            action='store',
            dest='questions',
            default=0,
            help='Number of questions to generate'),
        
        make_option('--answers',
            action='store',
            dest='answers',
            default=0,
            help='Number of answers to generate'),
        )

	def handle(self, *args, **options):
		names = {}
		while(len(names.keys())<int(options['users'])):
			names[get_username(length=30)]=1
		
		for name in names.keys():
			u = User.objects.create(username = name, email=get_email())
			p = Profile.objects.create(user_id = u.id, rating = random.randint(0,20))

		p_min = Profile.objects.all().aggregate(Min('id'))['id__min']
		p_max = Profile.objects.all().aggregate(Max('id'))['id__max']
		
		for i in range(0, int(options['questions'])):
			q = Question.objects.create(author_id = random.randint(p_min, p_max),
				title = sentence()[0:59],
				text = sentences(3))


