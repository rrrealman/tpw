from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView
from ask_pupkin.views import hw, gpp, index, login, signup

urlpatterns = patterns('',

    url(r'^admin/', include(admin.site.urls)),
    url(r'^django/hw$', hw),
    url(r'^django/gpp$', gpp),
    
    url(r'^$', index, {'questions_order': 0}),
    url(r'login/', login),
    url(r'signup/', signup),

    url(r'by_date', index, {'questions_order': 1}),
    url(r'by_rating', index, {'questions_order': 2}),   
)
