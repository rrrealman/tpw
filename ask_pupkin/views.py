from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from ask.models import Question, Profile, User

def hw(request):
	return HttpResponse("Django Hello World!")

def gpp(request):
	output = []
	for key, value in request.META.items():
		output.append("{0}: {1}<br>".format(key, value))
	return HttpResponse(output)

#=======================================================

def index(request, questions_order): 
	best_members = User.objects.order_by('-profile__rating')[:5] 

	if not questions_order: 
		paginator = Paginator(Question.objects.all(), 4)
	elif questions_order == 1:
		paginator = Paginator(Question.objects.order_by('-date_added'), 4)
	elif questions_order == 2:
		paginator = Paginator(Question.objects.order_by('-author__rating'), 4)
	
	page = request.GET.get('page')
	try:
		questions = paginator.page(page)
	except PageNotAnInteger:
		questions = paginator.page(1)
	except EmptyPage:
		questions = paginator.page(paginator.num_page)

	return render_to_response('index.html', {'qs':questions, 'best_ms':best_members, })

def login(request):
	return render_to_response('login.html')

def signup(request):
	return render_to_response('index.html')