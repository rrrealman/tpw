# routine prints http-request (GET or POST) params

def application(environ, start_response):
	status = '200 OK'
	response_headers = [('Content-type', 'text/plain')]
	
	output = []
	for key, value in environ.items():
		output.append("%s: %r" % (key, value))

	start_response(status, response_headers)
	return ["\n".join(output)]